package main

import (
	pb "github.com/miracle-1991/envoy-study/metrics/proto"
	"github.com/miracle-1991/envoy-study/metrics/service/metrics_service"
	"google.golang.org/grpc"
	"log"
	"net"
	"os"
)

func main() {
	address := ":" + os.Getenv("LocalGrpcPort")
	lis, err := net.Listen("tcp", address)
	if err != nil {
		log.Fatalf("failed to listen:%v", err)
	}

	grpcServer := grpc.NewServer()
	metricsService := new(metrics_service.MetricsService)
	pb.RegisterMetricsServiceServer(grpcServer, metricsService)
	grpcServer.Serve(lis)
}
