#!/bin/bash

rsa=`cat ~/.ssh/id_rsa`
echo "export SSH_PRIVATE_KEY='${rsa}'" >> ~/.bash_profile
source ~/.bash_profile