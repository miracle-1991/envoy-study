#!/bin/bash
#--service-node 定义运行Envoy的本地服务节点名称
#--service-cluster 定义运行Envoy的本地集群名称
./main &
#log-path 组件日志(string格式)的保存地址，访问日志的路径在yaml中配置
envoy -c /etc/service-envoy.yaml --service-cluster "service${SERVICE_NAME}" --log-path "/app/logs/application.log"