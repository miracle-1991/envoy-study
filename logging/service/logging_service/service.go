package logging_service

import (
	"context"
	"fmt"
	pb "github.com/miracle-1991/envoy-study/logging/proto"
	"os"
)

type LoggingService struct {
	pb.UnimplementedLogingServiceServer
}

func (s *LoggingService) Call(ctx context.Context, req *pb.StringRequest) (*pb.StringResponse, error) {
	fmt.Println("Route Request Start")
	defer func() {
		fmt.Println("Route Request End")
	}()

	//打印tracing信息
	serviceName := os.Getenv("SERVICE_NAME")
	log := fmt.Sprintf("service %s: recv grpc request, request is: %v", serviceName, req.Req)
	resp := &pb.StringResponse{
		Res: log,
	}
	fmt.Println(log)
	return resp, nil
}
