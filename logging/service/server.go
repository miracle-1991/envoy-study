package main

import (
	pb "github.com/miracle-1991/envoy-study/logging/proto"
	"github.com/miracle-1991/envoy-study/logging/service/logging_service"
	"google.golang.org/grpc"
	"log"
	"net"
	"os"
)

func main() {
	address := ":" + os.Getenv("LocalGrpcPort")
	lis, err := net.Listen("tcp", address)
	if err != nil {
		log.Fatalf("failed to listen:%v", err)
	}

	grpcServer := grpc.NewServer()
	loggingService := new(logging_service.LoggingService)
	pb.RegisterLogingServiceServer(grpcServer, loggingService)
	grpcServer.Serve(lis)
}
