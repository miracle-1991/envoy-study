#!/bin/bash

# 指定拉代码时使用的ssh rsa
rsa=`cat ~/.ssh/id_rsa`
echo "export SSH_PRIVATE_KEY='${rsa}'" >> ~/.bash_profile
source ~/.bash_profile

# 指定ELK的版本
export ELK_VERSION="7.3.1"

