package main

import (
	"context"
	"fmt"
	"github.com/gin-gonic/gin"
	pb "github.com/miracle-1991/envoy-study/logging/proto"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
	"net/http"
	"os"
)

func routeRpc(c *gin.Context) {
	fmt.Println("routeRpc Start")

	serviceAddr := ":" + os.Getenv("RemoteGrpcPort")
	conn, err := grpc.Dial(serviceAddr, grpc.WithInsecure())
	if err != nil {
		panic("connect error")
	}
	defer conn.Close()

	rpcClient := pb.NewLogingServiceClient(conn)
	stringReq := &pb.StringRequest{
		Req: "test request to " + serviceAddr,
	}

	routeKey := "service-node"
	var routeVal string
	if choose, ok := c.GetQuery("service"); ok {
		switch choose {
		case "1":
			routeVal = "service1"
		case "2":
			routeVal = "service2"
		case "3":
			routeVal = "service3"
		default:
			routeVal = "service1"
		}
	}

	md := metadata.Pairs(routeKey, routeVal)
	ctx := metadata.NewOutgoingContext(context.Background(), md)
	reply, err := rpcClient.Call(ctx, stringReq)
	if err != nil {
		fmt.Printf("MetricsService Error:%v\n", err)
		c.String(http.StatusBadRequest, err.Error())
		return
	}

	c.String(http.StatusOK, reply.Res)
	fmt.Println("routeRpc End")
}

func main() {
	address := ":" + os.Getenv("LocalHttpPort")
	fmt.Println("Front Listen Port:%v", address)
	r := gin.Default()
	r.GET("/logging", routeRpc)
	r.Run(address)
}
