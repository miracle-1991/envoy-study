package main

import (
	"context"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/miracle-1991/envoy-study/tracing/envoy-tracing/interceptor"
	pb "github.com/miracle-1991/envoy-study/tracing/envoy-tracing/proto"
	"google.golang.org/grpc"
	"net/http"
	"os"
)

// 发起rpc调用前不创建root span
func traceWithOutRootSpan(c *gin.Context) {
	fmt.Println("traceWithOutRootSpan Start")

	serviceAddr := ":" + os.Getenv("RemoteGrpcPort")
	conn, err := grpc.Dial(serviceAddr, grpc.WithInsecure(), grpc.WithChainUnaryInterceptor(
		interceptor.UnaryClientInterceptor(),
	))
	if err != nil {
		panic("connect error")
	}
	defer conn.Close()

	stringClient := pb.NewTraceServiceClient(conn)
	stringReq := &pb.StringRequest{
		Req: "traceWithOutRootSpan",
	}

	reply, err := stringClient.Trace(context.Background(), stringReq)
	if err != nil {
		fmt.Printf("TracingService Error:%v\n", err)
		c.String(http.StatusBadRequest, err.Error())
		return
	}

	c.String(http.StatusOK, reply.Res)
	fmt.Println("traceWithOutRootSpan End")
}

func main() {
	address := ":" + os.Getenv("LocalHttpPort")
	fmt.Println("Front Listen Port:%v", address)
	r := gin.Default()
	r.GET("/trace/1", traceWithOutRootSpan)
	r.Run(address)
}
