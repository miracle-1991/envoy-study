package tracing_service

import (
	"context"
	"fmt"
	"github.com/miracle-1991/envoy-study/tracing/envoy-tracing/interceptor"
	pb "github.com/miracle-1991/envoy-study/tracing/envoy-tracing/proto"
	"google.golang.org/grpc"
	"os"
)

type TracingService struct {
	pb.UnimplementedTraceServiceServer
}

// 发起rpc调用前不创建child span
func traceWithOutChildSpan(ctx context.Context) (*pb.StringResponse, error) {
	serviceAddr := ":" + os.Getenv("RemoteGrpcPort")
	conn, err := grpc.Dial(serviceAddr, grpc.WithInsecure(), grpc.WithChainUnaryInterceptor(
		interceptor.UnaryClientInterceptor(),
	))
	if err != nil {
		panic("connect error")
	}
	defer conn.Close()

	stringClient := pb.NewTraceServiceClient(conn)
	stringReq := &pb.StringRequest{
		Req: "traceWithOutChildSpan",
	}

	reply, err := stringClient.Trace(ctx, stringReq)
	if err != nil {
		return &pb.StringResponse{
			Res: fmt.Sprintf("service1 request error: %v", err.Error()),
		}, err
	}
	return reply, nil
}

func (s *TracingService) Trace(ctx context.Context, req *pb.StringRequest) (*pb.StringResponse, error) {
	fmt.Println("Trace Request Start")
	defer func() {
		fmt.Println("Trace Request End")
	}()

	//打印tracing信息
	serviceName := os.Getenv("SERVICE_NAME")
	if serviceName == "1" {
		fmt.Println("Service1: send grpc request")
		resp, err := traceWithOutChildSpan(ctx)
		resp.Res = "response from service1 is ok;" + resp.Res
		return resp, err
	} else {
		fmt.Println("Service2: recv grpc request")
		resp := &pb.StringResponse{
			Res: fmt.Sprintf("response from service2"),
		}
		return resp, nil
	}
}
