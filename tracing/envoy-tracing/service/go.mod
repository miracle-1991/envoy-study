module github.com/miracle-1991/envoy-study/tracing/envoy-tracing/service

go 1.18

require (
	github.com/miracle-1991/envoy-study/tracing/envoy-tracing/interceptor v0.0.0-20220530063338-64731aedb3e7
	github.com/miracle-1991/envoy-study/tracing/envoy-tracing/proto v0.0.0-20220530063338-64731aedb3e7
	google.golang.org/grpc v1.46.2
)

require (
	github.com/golang/protobuf v1.5.2 // indirect
	golang.org/x/net v0.0.0-20201031054903-ff519b6c9102 // indirect
	golang.org/x/sys v0.0.0-20210603081109-ebe580a85c40 // indirect
	golang.org/x/text v0.3.3 // indirect
	google.golang.org/genproto v0.0.0-20200526211855-cb27e3aa2013 // indirect
	google.golang.org/protobuf v1.28.0 // indirect
)
