package main

import (
	"github.com/miracle-1991/envoy-study/tracing/envoy-tracing/interceptor"
	pb "github.com/miracle-1991/envoy-study/tracing/envoy-tracing/proto"
	"github.com/miracle-1991/envoy-study/tracing/envoy-tracing/service/tracing_service"
	"google.golang.org/grpc"
	"log"
	"net"
	"os"
)

func main() {
	address := ":" + os.Getenv("LocalGrpcPort")
	lis, err := net.Listen("tcp", address)
	if err != nil {
		log.Fatalf("failed to listen:%v", err)
	}

	gopts := []grpc.ServerOption{}
	gopts = append(gopts, grpc.UnaryInterceptor(interceptor.UnaryServerInterceptor()))

	grpcServer := grpc.NewServer(gopts...)
	tracingService := new(tracing_service.TracingService)
	pb.RegisterTraceServiceServer(grpcServer, tracingService)
	grpcServer.Serve(lis)
}
