package interceptor

const (
	X_B3_TRACE_ID       = "x-b3-traceid"
	X_B3_SPAN_ID        = "x-b3-spanid"
	X_B3_PARENT_SPAN_ID = "x-b3-parentspanid"
	X_B3_SAMPLED        = "x-b3-sampled"
	X_B3_FLAGS          = "x-b3-flags"
)
