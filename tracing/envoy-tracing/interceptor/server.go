package interceptor

import (
	"context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
)

func extractContext(ctx context.Context, md metadata.MD, headerName string) context.Context {
	value := md[headerName]
	if len(value) >= 1 {
		ctx = context.WithValue(ctx, headerName, value[0])
	}
	return ctx
}

func UnaryServerInterceptor() grpc.UnaryServerInterceptor {
	return func(ctx context.Context, req interface{},
		info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (resp interface{}, err error) {
		md, ok := metadata.FromIncomingContext(ctx)
		if !ok {
			md = metadata.Pairs()
		}

		headers := []string{X_B3_TRACE_ID, X_B3_SPAN_ID, X_B3_PARENT_SPAN_ID, X_B3_SAMPLED, X_B3_FLAGS}
		for _, h := range headers {
			ctx = extractContext(ctx, md, h)
		}

		return handler(ctx, req)
	}
}
