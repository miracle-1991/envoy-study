# Envoy tracing测试程序
__本程序在安装了docker-compose的机器上执行__  
拓扑图:  
![tracing-test拓扑图](./docs/tracing-test-topology.png)

## 启动测试环境
```
docker-compose build
docker-compose up       
```
## 执行测试
```
curl -v localhost:8000/trace/1
```
### tracelog查看地址
```
http://localhost:16686/
```