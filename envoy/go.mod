module github.com/miracle-1991/envoy-study/envoy

go 1.18

require (
	github.com/envoyproxy/go-control-plane v0.10.1
	github.com/golang/protobuf v1.5.2
)

require (
	github.com/census-instrumentation/opencensus-proto v0.2.1 // indirect
	github.com/cncf/xds/go v0.0.0-20211001041855-01bcc9b48dfe // indirect
	github.com/envoyproxy/protoc-gen-validate v0.1.0 // indirect
	github.com/prometheus/client_model v0.0.0-20190812154241-14fe0d1b01d4 // indirect
	golang.org/x/net v0.0.0-20201021035429-f5854403a974 // indirect
	golang.org/x/sys v0.0.0-20210119212857-b64e53b001e4 // indirect
	golang.org/x/text v0.3.3 // indirect
	google.golang.org/genproto v0.0.0-20200526211855-cb27e3aa2013 // indirect
	google.golang.org/grpc v1.36.0 // indirect
	google.golang.org/protobuf v1.27.1 // indirect
)
