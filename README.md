# Envoy源码分析
***
__依赖Envoy源码，需要执行以下命令先下载envoy源代码__
```
 git submodule update --init      
```
执行此命令后，envoy代码会下载到本地的envoy目录中


## 使用gitbook查看此项目
### mac安装gitbook
1、 安装nvm  
nvm全名叫做node.js version management，是一个nodejs的版本管理工具。
可以通过它可以安装和切换不同版本的node.js
```
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash
```
2、安装nodejs  
注意不要下载最新版本，后续CLI安装gitbook会报错，测试10.22.0可以
```
nvm install 10.22.0
```
3、安装gitbook  
先安装gitbook命令行
```
sudo npm install -g gitbook-cli
```
使用gitbook继续安装
```
gitbook -V
```
再次执行gitbook -V,如果看到如下输出，则代表安装完成
```
CLI version: 2.3.2
GitBook version: 3.2.3
```
安装优化显示的插件
```
gitbook install
```
4、使用gitbook查看此项目  
```
gitbook serve
```
该命令会启动本地http服务，监听在4000端口：
```
Starting server ...
Serving book on http://localhost:4000
```
浏览器打开[http://localhost:4000](http://localhost:4000)即可查看此项目